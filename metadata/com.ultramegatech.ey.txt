Categories:Science & Education
License:MIT
Web Site:http://ultramegasoft.com
Source Code:https://github.com/ultramega/elementary
Issue Tracker:https://github.com/ultramega/elementary/issues
Donate:http://ey.ultramegatech.com/donate

Auto Name:Elementary
Summary:Periodic table of elements
Description:
Tap the element to see info and access videos and
Wikipedia articles.
.

Repo Type:git
Repo:https://github.com/ultramega/elementary.git

Build:0.1.5,6
    commit=eecdec

Build:0.1.6,7
    commit=99b8bf567797e085f6

Build:0.1.7,8
    commit=0.1.7

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.1.7
Current Version Code:8

